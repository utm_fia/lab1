from components import Rule, Or, And, Option, Facts


def get_rules():
    return [
        Rule(
            Or('breath', 'have blood', 'have oil'),
            None,
            Option('mechanical', ['No', 'No', 'Yes']),
            Option('biological', ['Yes', 'Yes', 'No'], 'All biological creature does have DNA')
        ),

        Rule(
            Or('have only one form', 'transform'),
            Facts('mechanical'),
            Option('transformer', ['No', 'Yes'], 'It chooses the form depending on the needs, to enhance its chances of winning in every situation'),
            Option('not transformer', ['Yes', 'No'])
        ),

        Rule(
            Or('have a behavior'),
            Facts('transformer'),
            Option('friendly', ['Friend']),
            Option('enemy', ['Enemy'])
        ),

        Rule(
            And('have intentions', 'have guns'),
            Facts('not transformer'),
            Option('ex criminal', ['Bad', 'No']),
            Option('criminal', ['Bad', 'Yes']),
            Option('policeman', ['Good', 'Yes']),
            Option('civilian', ['Good', 'No'])
        ),

        Rule(
            Or('teleport'),
            Facts('not transformer'),
            Option('teleporter', ['Yes']),
            Option('not teleporter', ['No'])
        ),

        Rule(
            Or('have feathers', 'fly', 'walk'),
            Facts('biological'),
            Option('flier', ['Yes', 'Yes', 'No'], 'It never walks'),
            Option('walker', ['No', 'No', 'Yes']),
        ),

        Rule(
            And('have a face shape', 'have superpowers'),
            Facts('biological'),
            Option('wizard', ['Round', 'Yes']),
            Option('exynos', ['Round', 'No']),
            Option('squid', ['Triangle', 'No']),
            Option('shaman', ['Triangle', 'Yes']),
        ),

        Rule(
            None,
            Facts('friendly'),
            Option('Autobot', None, 'This magnific creature is from the planet Cybertron in the galaxy X720')
        ),

        Rule(
            None,
            Facts('enemy'),
            Option('Decepticon', None, 'It is from galaxy X720')
        ),

        Rule(
            None,
            Facts('teleporter', 'policeman'),
            Option('Mangu', None, 'It is from galaxy X720')
        ),

        Rule(
            None,
            Facts('teleporter', 'civilian'),
            Option('Frawli', None, 'It is from galaxy X720')
        ),

        Rule(
            None,
            Facts('teleporter', 'criminal'),
            Option('Didun', None, 'It is from galaxy X720'),
        ),

        Rule(
            None,
            Facts('not teleporter', 'civilian'),
            Option('Sili', None, 'It is from galaxy Ardbei')
        ),

        Rule(
            None,
            Facts('teleporter', 'ex criminal'),
            Option('Sakay', None, 'It is from galaxy Ardbei')
        ),

        Rule(
            None,
            Facts('not teleporter', 'policeman'),
            Option('Limun', None, 'It is from galaxy Kotakota')
        ),

        Rule(
            None,
            Facts('not teleporter', 'criminal'),
            Option('Mandimu', None, 'It is from galaxy Kotakota')
        ),

        Rule(
            None,
            Facts('not teleporter', 'ex criminal'),
            Option('Remana', None, 'It is from galaxy Kotakota')
        ),

        Rule(
            None,
            Facts('flier', 'wizard'),
            Option('Shelton', None, 'It is from galaxy TresLeches')
        ),

        Rule(
            None,
            Facts('flier', 'exynos'),
            Option('Maelk', None, 'It is from galaxy TresLeches')
        ),

        Rule(
            None,
            Facts('flier', 'squid'),
            Option('Mlijeko', None, 'It is from galaxy TresLeches')
        ),

        Rule(
            None,
            Facts('flier', 'shaman'),
            Option('Skeleon', None, 'It is from galaxy TresLeches')
        ),

        Rule(
            None,
            Facts('walker', 'exynos'),
            Option('Human', None, 'It is from galaxy Milky Way')
        ),

        Rule(
            None,
            Facts('walker', 'shaman'),
            Option('Saturnian', None, 'It is from galaxy Milky Way')
        ),

        Rule(
            None,
            Facts('walker', 'squid'),
            Option('Marsian', None, 'It is from galaxy Milky Way')
        ),

        Rule(
            None,
            Facts('walker', 'wizard'),
            Option('Lunian', None, 'It is from galaxy Milky Way')
        ),
    ]
