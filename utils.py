def choose_rule(facts, rules):
    fact_rule_list = []
    for rule in rules:
        if rule.facts.match(facts, rules):
            fact_rule_list.append(rule)
    return fact_rule_list


def generate_new_facts(answers, options, condition, list_questions):
    if answers:
        if condition.is_or():
            user_answer = list_questions[0].answers[answers[0] - 1]
            for option in options:
                question_index = condition.args.index(list_questions[0].question)
                if option.variants[question_index] == user_answer:
                    return option.fact
        else:
            all_answers = [None] * len(list_questions)
            for index, question in enumerate(list_questions):
                question_index = condition.args.index(question.question)
                ans = question.answers[answers[index] - 1]
                all_answers[question_index] = ans
                for option in options:
                    if option.variants == all_answers:
                        return option.fact
    else:
        return None
