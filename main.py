from commands import print_commands, execute_command


if __name__ == "__main__":
    print_commands()
    while True:
        command = input("Write a command:")
        execute_command(command)
        print()
