import random


class Rule:
    def __init__(self, condition, facts, *options):
        self.condition = condition
        if not facts:
            facts = Facts()
        self.facts = facts
        self.options = options
        self.used_questions = []

    def __repr__(self):
        if self.condition:
            return f'{self.condition.args}{self.facts}'
        return f'{self.facts}'

    def is_goal(self):
        return any(option.is_goal() for option in self.options)

    def get_questions(self):
        question_list = []

        if self.condition.is_or():
            question = random.choice(self.get_available_questions())
            question_index = self.condition.args.index(question)
            self.used_questions.append(question_index)
            answers = self.get_answers(question_index)
            question_list.append(Question(question, list(set(answers))))
        else:
            for index, question in enumerate(self.condition.args):
                answers = self.get_answers(index)
                question_list.append(Question(question, list(set(answers))))
        random.shuffle(question_list)
        return question_list

    def get_answers(self, question_index):
        answers = []
        for option in self.options:
            answers.append(option.variants[question_index])
        return answers

    def get_available_questions(self):
        return [element for i, element in enumerate(self.condition.args) if i not in self.used_questions]


class Condition:
    def __init__(self, operator, args):
        self.operator = operator
        self.args = args

    def is_or(self):
        return self.operator == "OR"


class Or(Condition):
    def __init__(self, *args):
        Condition.__init__(self, 'OR', args)


class And(Condition):
    def __init__(self, *args):
        Condition.__init__(self, 'AND', args)


class Option:
    def __init__(self, fact, variants=None, info=None):
        self.fact = fact
        self.variants = variants
        self.info = info

    def is_goal(self):
        return self.variants is None


class Facts:
    def __init__(self, *facts):
        if not facts:
            facts = ()
        self.facts_list = list(facts)

    def __repr__(self):
        return f'{self.facts_list}'

    def match(self, facts, rules):
        all_facts = gather_facts(self.facts_list, rules)
        all_facts.sort()
        facts.sort()
        return all_facts == facts


class Question:
    def __init__(self, question, answers):
        self.question = question
        self.answers = answers

    def ask_question(self):
        question = "Does it " + self.question + "?"
        print(question)
        for index, answer in enumerate(self.answers):
            print(f"{index + 1}. {answer}")
        print(f"{len(self.answers) + 1}. Don't know")

    def __repr__(self):
        return f'{self.question}:{self.answers}'


def gather_facts(facts, rules):
    has_new_facts = True
    while has_new_facts:
        has_new_facts = False
        for rule in rules:
            for option in rule.options:
                if option.fact in facts:
                    for rule_fact in rule.facts.facts_list:
                        if rule_fact not in facts:
                            facts.append(rule_fact)
                            has_new_facts = True
    return facts
