from utils import choose_rule, generate_new_facts
from components import gather_facts
from rules import get_rules


def backward_chaining(type_of_creature):
    rules = get_rules()
    if not find_rule_by_fact(type_of_creature, rules):
        raise
    print()
    print(f"All the facts about {type_of_creature}:")
    tourist_facts = gather_facts([type_of_creature], rules)
    tourist_rules = [find_rule_by_fact(fact, rules) for fact in tourist_facts]
    tourist = tourist_facts.pop(0)
    t_rule = tourist_rules.pop(0)
    print(f'It is {tourist} because: ')
    for rule, fact in zip(tourist_rules, tourist_facts):
        print_rule_of_fact(fact, rule)
    option = find_option_by_fact(tourist, t_rule)
    if option.info:
        print(f'Interesting fact about {tourist}: {option.info}')


def print_rule_of_fact(fact, rule):
    option = find_option_by_fact(fact, rule)
    print(f'\tIt is {fact} because: ')
    if option.variants:
        for question, variant in zip(rule.condition.args, option.variants):
            print(f'\t\tit does {question}: {variant}')
    if rule.facts and rule.facts.facts_list and len(rule.facts.facts_list) > 0:
        print(f'\t\tand is {",".join(rule.facts.facts_list)}')
    if option.info:
        print(f'\tInteresting fact about {fact}: {option.info}')
    print()


def find_rule_by_fact(fact, rules):
    for r in rules:
        if find_option_by_fact(fact, r):
            return r


def find_option_by_fact(fact, rule):
    for o in rule.options:
        if fact == o.fact:
            return o


def forward_chaining():
    facts = []
    rules = get_rules()
    while True:
        intermediate_rules = choose_rule(facts, rules)
        if len(intermediate_rules) == 0:
            raise
        if any(rule.is_goal() for rule in intermediate_rules):
            print(f"Result: {next(rule for rule in intermediate_rules if rule.is_goal()).options[0].fact}")
            facts.clear()
            break
        for rule in intermediate_rules:
            work_rule(facts, rule)


def work_rule(facts, rule):
    condition = rule.condition
    options = rule.options
    list_questions = rule.get_questions()
    answers = []
    for question in list_questions:
        invalid = True
        while invalid:
            try:
                question.ask_question()
                answer = int(input())
                if answer in range(1, len(question.answers) + 2):
                    invalid = False
                    if answer != len(question.answers) + 1:
                        answers.append(answer)
                else:
                    raise Exception
            except:
                print("Incorrect input. Try once again")
    new_fact = generate_new_facts(answers, options, condition, list_questions)
    if new_fact is not None:
        facts.append(new_fact)
    print(f"Current information: {facts}")
