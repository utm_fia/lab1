# Laboratory nr.1 - Expert System
#### Table of contents
* [Introduction](#introduction)
* [Available commands](#available-commands)
* [Technologies](#technologies)
* [Setup](#setup)
* [Examples](#examples)
* [Resources](#resources)

## Introduction
The main task of the laboratory work nr.1 is to develop an Expert System which is going to solve the problem of toursit detection in Luna-City. 
The system is able to distinguish between 18 types of creatures: 17 types of toursits and 1 type of local citizens.

In order to be able to detect the type of creature, the forward chaining algorithm was used. For that, specific questions are asked thus gathering data from user. Based on that data, the inference rules are applied. They help to extract more data about the creatures and to reach the final goal.

Contrariwise, for gathering all the available facts of the creature based on a hypothesis, the backward chaining algorithm is used. The main idea is to start from the final goal and chain through inference rules in order to find new facts that satisfy it. Also, it provides some additional information about the origin of creature.

All the defined rules and data can be found in the ```rules.py``` file.

The structure of a rule is:
* Condition - an abstract base class used to define 2 types of conditions: *AND* and *OR*. In the case of *AND*, all the facts should be asked, while for the *OR* it is enough just one which is chosen by random.
* Facts - a class that contains a list of facts that should be already satisfyed in order to be able to apply the certain rule.
* Options - a class used to define the potential new fact based on answers of the user.

Below is presented an example:
```python
Rule(
    And('have a face shape', 'have superpowers'),
    Facts('biological'),
    Option('wizard', ['Round', 'Yes']),
    Option('exynos', ['Round', 'No']),
    Option('squid', ['Triangle', 'No']),
    Option('shaman', ['Triangle', 'Yes']),
)
```

## Available commands
The current system supports the following commands:
* ***detect creature*** - used to return the type of creature based on answers to specific questions
* ***get info about creature*** - used to show all the known facts about the specific type of creature
* ***help*** - used to show information regarding available commands
* ***exit*** - used to exit the program

## Technologies
Used technologies:
* Python 3.7

## Setup
In order to run the project run in the terminal:

```python main.py```

## Examples
### detect creature

This command implies the use of forward chaining algorithm. After each answered question, all the already known facts
regarding the creature are shown to the user. The end result represents the final goal.

![detect creature](screenshots/detect_creature.png)


### get info about creature

This command implies the use of backward chaining algorithm. Based on the final goal, it returns all known facts about
chosen creature. Also, it provides some additional information regarding the species.

![get info about creature](screenshots/get_info_about_creature.png)


### help

![help](screenshots/help.png)

## Resources
UTM Fundamentals of Artificial Intelligence Course
