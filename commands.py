import sys
from chainings import forward_chaining, backward_chaining


def show_available_commands():
    print("-----------Commands---------")
    print("1. detect creature           -  return the type of creature based on answers to specific questions")
    print("2. get info about creature   -  show all the known facts about the specific type of creature")
    print("3. help                      -  show information regarding available commands")
    print("4. exit                      -  exit the program")


def print_commands():
    print()
    print("-----------Commands---------")
    print("1. detect creature")
    print("2. get info about creature")
    print("3. help")
    print("4. exit")


def execute_command(user_command):
    if user_command == "1":
        try:
            forward_chaining()
        except:
            print("Not enough information")
        print_commands()
    elif user_command == "2":
        try:
            type_of_creature = input("Type of creature:")
            backward_chaining(type_of_creature)
        except:
            print("No creature found")
        print_commands()
    elif user_command == "3":
        show_available_commands()
    elif user_command == "4":
        sys.exit(0)
    else:
        print("Unavailable command. Try again")
